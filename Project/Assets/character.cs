using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class character : MonoBehaviour
{
    private Transform ThisTransform = null;
    public float MoveSpeed = 1f;
    public float RotSpeed = 45f;
    public GameObject gameOverUI;
    public GameObject gameWinUI;
    private Touch touch;
    public float speedModifier;
    public Animator myAnim;

    void Awake(){
        ThisTransform = GetComponent<Transform>();
    }

    void Start()
    {
        speedModifier = 0.07f;
    }

    // Update is called once per frame
    void Update()
    {
        float Horz= Input.GetAxis("Horizontal");
        float Vert = Input.GetAxis("Vertical");

        ThisTransform.Rotate(0,RotSpeed * Time.deltaTime * Horz,0f);
        ThisTransform.position += ThisTransform.forward * MoveSpeed * Time.deltaTime * Vert;
    if(Input.touchCount>0){
        touch = Input.GetTouch(0);
        if(touch.phase == TouchPhase.Moved){
            transform.position = new Vector3(
                transform.position.x+ touch.deltaPosition.x * speedModifier,
                transform.position.y,
                transform.position.z+touch.deltaPosition.y * speedModifier

            );
        }
    }
    }
     void OnCollisionEnter(Collision other) {
       if(other.gameObject.tag == "GameOver"){
           Destroy(gameObject);
           gameOverUI.SetActive(true);
       }
       else if(other.gameObject.tag == "Finish"){
           myAnim.Play("characteractiontoidle");
           gameWinUI.SetActive(true);
       }
    }
}
