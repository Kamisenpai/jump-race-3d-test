using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platform : MonoBehaviour
{
   
    public float jumpForce = 10f;
 void OnCollisionEnter(Collision other) {
        Rigidbody rb = other.collider.GetComponent<Rigidbody>();
        if (rb != null){
            Vector3 velocity = rb.velocity;
            velocity.y = jumpForce;
            rb.velocity = velocity;
            
        }
    }
}
