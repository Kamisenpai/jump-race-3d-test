using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wordTrigger : MonoBehaviour
{
    public GameObject Wordtrigger;
    public float duration = 1f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
     void OnTriggerEnter(Collider other) {
            if(other.tag == "Player"){
               
                StartCoroutine(WordTriggerCoroutine());
          
        }
        }
         
        IEnumerator WordTriggerCoroutine()
    {
         Wordtrigger.SetActive(true);

        yield return new WaitForSeconds(duration);
        Wordtrigger.SetActive(false);
        
    }
}
