using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public GameObject playUI;

    void Start(){
        Time.timeScale = 0f;
    }
    public void PlayGame(){
       playUI.SetActive(false);
       Time.timeScale = 1f;
    }
   public void ReplayGame(){
        SceneManager.LoadScene("GameScene");
    }
    public void QuitGame(){
        Application.Quit();
    }
}
