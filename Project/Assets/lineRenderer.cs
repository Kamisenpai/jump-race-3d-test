using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lineRenderer : MonoBehaviour
{
    public LineRenderer lr;
    private RaycastHit hit;
      Color c1 = Color.white;
    Color c2 = new Color(1, 1, 1, 0);

     void Start()
    {
        lr.material = new Material(Shader.Find("Sprites/Default"));
        lr.SetColors(c1, c2);
    } 

    // Update is called once per frame
    void Update()
    {
        lr.SetPosition(0, new Vector3(0, 0.5f, 0));
        if (Physics.Raycast(transform.position, -transform.up, out hit))
        {
            if (hit.collider.CompareTag("Platform"))
            {
                lr.SetPosition(1,transform.InverseTransformPoint(hit.point));
                lr.material.color = Color.blue;

            }
            else
            {
                lr.SetPosition(1, -transform.up * 100);
                lr.material.color = Color.red;
                

            }
        }
        else
        {
            lr.SetPosition(1, -transform.up * 100);
            lr.material.color = Color.red;
        }
        
        var tempColor = lr.material.color;
        tempColor.a = 0.5f;
        lr.material.color = tempColor;
    }
}
