using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platformFall : MonoBehaviour
{

bool isFalling = false;
public Animator myAnim;
float downSpeed =0;
public float currentHealth ;
public float initialHealth =3;

public float damage = 1;

 void Start()
    {
        currentHealth = initialHealth ;
       
    }

    void OnTriggerEnter(Collider collider){
        if(collider.tag == "Player"){
            myAnim.Play("touched");
            currentHealth -= damage;
            if(currentHealth <= 0){
                isFalling = true;
            myAnim.Play("Destroyed");
            Destroy(gameObject,3);
            }
           

        }
    }

    // Update is called once per frame
    void Update()
    {
        if(isFalling){
            downSpeed += Time.deltaTime/20;
            transform.position = new Vector3(transform.position.x,
            transform.position.y-downSpeed,
            transform.position.z);
        }
    }
}
